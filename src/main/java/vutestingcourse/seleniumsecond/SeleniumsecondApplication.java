package vutestingcourse.seleniumsecond;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeleniumsecondApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeleniumsecondApplication.class, args);
    }

}
