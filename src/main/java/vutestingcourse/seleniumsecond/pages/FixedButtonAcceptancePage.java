package vutestingcourse.seleniumsecond.pages;

import org.openqa.selenium.*;

public class FixedButtonAcceptancePage {
    private static final String URL = "https://suninjuly.github.io/alert_accept.html";
    private final WebDriver driver;

    private final By buttonSelector = By.cssSelector(".btn");

    private WebElement button;
    private Alert alert;

    public FixedButtonAcceptancePage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get(URL);

        button = driver.findElement(buttonSelector);
    }

    public String getButtonText() {
        return button.getText();
    }

    public void clickButtonAndSwitchToAlert() {
        button.click();
        alert = driver.switchTo().alert();
    }

    public String getAlertText() {
        if (alert == null) {
            throw new IllegalStateException("Button was not clicked. Alert was not fetched");
        }
        return alert.getText();
    }

    public void confirmAlert() {
        if (alert == null) {
            throw new IllegalStateException("Button was not clicked. Alert was not fetched");
        }
        alert.accept();
    }
}
