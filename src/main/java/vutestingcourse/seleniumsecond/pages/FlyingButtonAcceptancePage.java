package vutestingcourse.seleniumsecond.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class FlyingButtonAcceptancePage {
  private static final String URL = "https://suninjuly.github.io/redirect_accept.html";
  private final WebDriver driver;

  private final By buttonSelector = By.cssSelector(".trollface");
  private WebElement button;

  public FlyingButtonAcceptancePage(WebDriver driver) {
    this.driver = driver;
  }

  public void open() {
    driver.get(URL);

    button = driver.findElement(buttonSelector);
  }

  public String getButtonText() {
    return button.getText();
  }

  public void clickButtonAndSwitchWindow() {
    button.click();

    String parentWindow = driver.getWindowHandle();
    for (String windowHandle : driver.getWindowHandles()) {
      if (!windowHandle.equals(parentWindow)) {
        driver.switchTo().window(windowHandle);
      }
    }
  }
}
