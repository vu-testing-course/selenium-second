package vutestingcourse.seleniumsecond.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AlertRedirectPage {
  private final WebDriver driver;

  private final By variableSelector = By.id("input_value");
  private final By inputFieldSelector = By.id("answer");
  private final By submitSelector = By.className("btn");

  private WebElement variable;
  private WebElement inputField;
  private WebElement submit;

  public AlertRedirectPage(WebDriver driver) {
    this.driver = driver;
  }

  public void getFields() {
    variable = driver.findElement(variableSelector);
    inputField = driver.findElement(inputFieldSelector);
    submit = driver.findElement(submitSelector);
  }

  public void inputSolution() {
    inputField.sendKeys(getSolution());
    submit.click();
  }

  public String getCongratulationText() {
    Alert alert = driver.switchTo().alert();
    String result = alert.getText();
    alert.accept();
    return result;
  }

  private String getSolution() {
    double number = Double.parseDouble(variable.getText());
    double solution = Math.log(Math.abs(12 * Math.sin(number)));
    return String.valueOf(solution);
  }
}
