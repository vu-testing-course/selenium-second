package vutestingcourse.seleniumsecond.flow;

import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import vutestingcourse.seleniumsecond.pages.AlertRedirectPage;
import vutestingcourse.seleniumsecond.pages.FlyingButtonAcceptancePage;

import java.time.Duration;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FlyingButtonAlertRedirectFlowTest {
  public static WebDriver driver;
  public static FlyingButtonAcceptancePage acceptancePage;
  public static AlertRedirectPage redirectPage;

  @BeforeAll
  public static void setup() {
    driver = new ChromeDriver();
    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    driver.manage().window().maximize();

    acceptancePage = new FlyingButtonAcceptancePage(driver);
    redirectPage = new AlertRedirectPage(driver);

    acceptancePage.open();
  }

  @AfterAll
  public static void tearDown() {
    driver.close();
    driver.quit();
  }

  @Test
  @Order(1)
  public void verifyButtonText() {
    String expected = "I want to go on a magical journey!";
    String actual = acceptancePage.getButtonText();
    Assertions.assertEquals(expected, actual);
  }

  @Test
  @Order(2)
  public void verifyRedirectedPageUrl() {
    acceptancePage.clickButtonAndSwitchWindow();

    String expected = "https://suninjuly.github.io/redirect_page.html?";
    String actual = driver.getCurrentUrl();
    Assertions.assertEquals(expected, actual);
  }

  @Test
  @Order(3)
  public void verifySolution() {
    redirectPage.getFields();
    redirectPage.inputSolution();

    String expected =
        "Congrats, you've passed the task! Copy this code as the answer to Stepik quiz";
    String actual = redirectPage.getCongratulationText().split(": ")[0];
    Assertions.assertEquals(expected, actual);
  }
}
