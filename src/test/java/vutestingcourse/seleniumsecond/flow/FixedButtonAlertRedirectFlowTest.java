package vutestingcourse.seleniumsecond.flow;

import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import vutestingcourse.seleniumsecond.pages.FixedButtonAcceptancePage;
import vutestingcourse.seleniumsecond.pages.AlertRedirectPage;

import java.time.Duration;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FixedButtonAlertRedirectFlowTest {
  public static WebDriver driver;
  public static FixedButtonAcceptancePage acceptancePage;
  public static AlertRedirectPage redirectPage;

  @BeforeAll
  public static void setup() {
    driver = new ChromeDriver();
    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    driver.manage().window().maximize();

    acceptancePage = new FixedButtonAcceptancePage(driver);
    redirectPage = new AlertRedirectPage(driver);

    acceptancePage.open();
  }

  @AfterAll
  public static void tearDown() {
    driver.close();
    driver.quit();
  }

  @Test
  @Order(1)
  public void verifyButtonText() {
    String expected = "I want to go on a magical journey!";
    String actual = acceptancePage.getButtonText();
    Assertions.assertEquals(expected, actual);
  }

  @Test
  @Order(2)
  public void verifyAlertText() {
    acceptancePage.clickButtonAndSwitchToAlert();

    String expected = "Are you sure?";
    String actual = acceptancePage.getAlertText();
    Assertions.assertEquals(expected, actual);
  }

  @Test
  @Order(3)
  public void verifyRedirectedPageUrl() {
    acceptancePage.confirmAlert();

    String expected = "https://suninjuly.github.io/alert_redirect.html?";
    String actual = driver.getCurrentUrl();
    Assertions.assertEquals(expected, actual);
  }

  @Test
  @Order(4)
  public void verifySolution() {
    redirectPage.getFields();
    redirectPage.inputSolution();

    String expected =
        "Congrats, you've passed the task! Copy this code as the answer to Stepik quiz";
    String actual = redirectPage.getCongratulationText().split(": ")[0];
    Assertions.assertEquals(expected, actual);
  }
}
